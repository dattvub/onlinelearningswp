﻿using System;
using System.Collections.Generic;

namespace OnlineLearning.Models
{
    public partial class Answer
    {
        public Answer()
        {
            FlashCards = new HashSet<FlashCard>();
        }

        public int AnswerId { get; set; }
        public string Content { get; set; } = null!;

        public virtual ICollection<FlashCard> FlashCards { get; set; }
    }
}

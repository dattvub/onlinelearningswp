﻿using System;
using System.Collections.Generic;

namespace OnlineLearning.Models
{
    public partial class Course
    {
        public Course()
        {
            FlashCards = new HashSet<FlashCard>();
            ProfileCourses = new HashSet<ProfileCourse>();
            Tests = new HashSet<Test>();
        }

        public int CourseId { get; set; }
        public string Title { get; set; } = null!;
        public string? Description { get; set; }
        public double? Rate { get; set; }
        public int? NumberFlashcard { get; set; }
        public int? SubjectId { get; set; }
        public int? ProfileId { get; set; }
        public bool? Status { get; set; }
        public int? IsEnrolled { get; set; }

        public virtual Subject? Subject { get; set; }
        public virtual ICollection<FlashCard> FlashCards { get; set; }
        public virtual ICollection<ProfileCourse> ProfileCourses { get; set; }
        public virtual ICollection<Test> Tests { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace OnlineLearning.Models
{
    public partial class FlashCard
    {
        public FlashCard()
        {
            Codes = new HashSet<Test>();
        }

        public int FlashcardId { get; set; }
        public string Question { get; set; } = null!;
        public int CourseId { get; set; }
        public int AnswerId { get; set; }
        public bool? Status { get; set; }

        public virtual Answer Answer { get; set; } = null!;
        public virtual Course Course { get; set; } = null!;

        public virtual ICollection<Test> Codes { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace OnlineLearning.Models
{
    public partial class SWPContext : DbContext
    {
        public SWPContext()
        {
        }

        public SWPContext(DbContextOptions<SWPContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Account> Accounts { get; set; } = null!;
        public virtual DbSet<Answer> Answers { get; set; } = null!;
        public virtual DbSet<Comment> Comments { get; set; } = null!;
        public virtual DbSet<Course> Courses { get; set; } = null!;
        public virtual DbSet<FlashCard> FlashCards { get; set; } = null!;
        public virtual DbSet<Profile> Profiles { get; set; } = null!;
        public virtual DbSet<ProfileCourse> ProfileCourses { get; set; } = null!;
        public virtual DbSet<Role> Roles { get; set; } = null!;
        public virtual DbSet<Subject> Subjects { get; set; } = null!;
        public virtual DbSet<Test> Tests { get; set; } = null!;
        public virtual DbSet<TestProfile> TestProfiles { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Data Source=DESKTOP-JTKT3KO\\DAT;Initial Catalog=SWP;User ID=sa;Password=123456;Trust Server Certificate=true");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>(entity =>
            {
                entity.ToTable("Account");

                entity.Property(e => e.AccountId)
                    .ValueGeneratedNever()
                    .HasColumnName("account_id");

                entity.Property(e => e.Blocked).HasColumnName("blocked");

                entity.Property(e => e.ConfirmationToken)
                    .HasMaxLength(250)
                    .HasColumnName("confirmationToken");

                entity.Property(e => e.Confirmed).HasColumnName("confirmed");

                entity.Property(e => e.Email)
                    .HasMaxLength(250)
                    .HasColumnName("email");

                entity.Property(e => e.Hash)
                    .HasMaxLength(250)
                    .HasColumnName("hash");

                entity.Property(e => e.IsActivated).HasColumnName("isActivated");

                entity.Property(e => e.Password)
                    .HasMaxLength(250)
                    .HasColumnName("password");

                entity.Property(e => e.ResetPasswordToken)
                    .HasMaxLength(250)
                    .HasColumnName("resetPasswordToken");

                entity.Property(e => e.RoleId).HasColumnName("role_id");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.Accounts)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Account_Role");
            });

            modelBuilder.Entity<Answer>(entity =>
            {
                entity.ToTable("Answer");

                entity.Property(e => e.AnswerId)
                    .ValueGeneratedNever()
                    .HasColumnName("answer_id");

                entity.Property(e => e.Content).HasMaxLength(250);
            });

            modelBuilder.Entity<Comment>(entity =>
            {
                entity.ToTable("Comment");

                entity.Property(e => e.CommentId)
                    .ValueGeneratedNever()
                    .HasColumnName("comment_id");

                entity.Property(e => e.Content)
                    .HasMaxLength(250)
                    .HasColumnName("content");

                entity.Property(e => e.CourseId).HasColumnName("course_id");

                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasColumnName("createdAt");

                entity.Property(e => e.ProfileId).HasColumnName("profile_id");

                entity.Property(e => e.Status).HasColumnName("status");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnType("datetime")
                    .HasColumnName("updatedAt");

                entity.HasOne(d => d.Profile)
                    .WithMany(p => p.Comments)
                    .HasForeignKey(d => d.ProfileId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Comment_Profile");
            });

            modelBuilder.Entity<Course>(entity =>
            {
                entity.ToTable("Course");

                entity.Property(e => e.CourseId).HasColumnName("course_id");

                entity.Property(e => e.Description)
                    .HasMaxLength(250)
                    .HasColumnName("description");

                entity.Property(e => e.IsEnrolled).HasColumnName("isEnrolled");

                entity.Property(e => e.NumberFlashcard).HasColumnName("number_flashcard");

                entity.Property(e => e.ProfileId).HasColumnName("profile_id");

                entity.Property(e => e.Rate).HasColumnName("rate");

                entity.Property(e => e.Status).HasColumnName("status");

                entity.Property(e => e.SubjectId).HasColumnName("subject_id");

                entity.Property(e => e.Title)
                    .HasMaxLength(250)
                    .HasColumnName("title");

                entity.HasOne(d => d.Subject)
                    .WithMany(p => p.Courses)
                    .HasForeignKey(d => d.SubjectId)
                    .HasConstraintName("FK_Course_Subject");
            });

            modelBuilder.Entity<FlashCard>(entity =>
            {
                entity.ToTable("FlashCard");

                entity.Property(e => e.FlashcardId).HasColumnName("flashcard_id");

                entity.Property(e => e.AnswerId).HasColumnName("answer_id");

                entity.Property(e => e.CourseId).HasColumnName("course_id");

                entity.Property(e => e.Question)
                    .HasMaxLength(2000)
                    .HasColumnName("question");

                entity.Property(e => e.Status).HasColumnName("status");

                entity.HasOne(d => d.Answer)
                    .WithMany(p => p.FlashCards)
                    .HasForeignKey(d => d.AnswerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FlashCard_Answer");

                entity.HasOne(d => d.Course)
                    .WithMany(p => p.FlashCards)
                    .HasForeignKey(d => d.CourseId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FlashCard_Course");
            });

            modelBuilder.Entity<Profile>(entity =>
            {
                entity.ToTable("Profile");

                entity.Property(e => e.ProfileId)
                    .ValueGeneratedNever()
                    .HasColumnName("profile_id");

                entity.Property(e => e.Address)
                    .HasMaxLength(250)
                    .HasColumnName("address");

                entity.Property(e => e.Birthday)
                    .HasColumnType("date")
                    .HasColumnName("birthday");

                entity.Property(e => e.Blocked).HasColumnName("blocked");

                entity.Property(e => e.Email)
                    .HasMaxLength(250)
                    .HasColumnName("email");

                entity.Property(e => e.Gender).HasColumnName("gender");

                entity.Property(e => e.Name)
                    .HasMaxLength(250)
                    .HasColumnName("name");

                entity.Property(e => e.Phone)
                    .HasMaxLength(10)
                    .HasColumnName("phone");

                entity.Property(e => e.RoleId).HasColumnName("role_id");

                entity.Property(e => e.Status).HasColumnName("status");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.Profiles)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Profile_Role");
            });

            modelBuilder.Entity<ProfileCourse>(entity =>
            {
                entity.HasKey(e => new { e.ProfileId, e.CourseId });

                entity.ToTable("Profile_Course");

                entity.Property(e => e.ProfileId).HasColumnName("profile_id");

                entity.Property(e => e.CourseId).HasColumnName("course_id");

                entity.Property(e => e.NumberFlashcard).HasColumnName("number_flashcard");

                entity.Property(e => e.Process).HasColumnName("process");

                entity.Property(e => e.TimeEnroll)
                    .HasColumnType("datetime")
                    .HasColumnName("time_enroll");

                entity.HasOne(d => d.Course)
                    .WithMany(p => p.ProfileCourses)
                    .HasForeignKey(d => d.CourseId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Profile_Course_Course");

                entity.HasOne(d => d.Profile)
                    .WithMany(p => p.ProfileCourses)
                    .HasForeignKey(d => d.ProfileId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Profile_Course_Profile");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.ToTable("Role");

                entity.Property(e => e.RoleId)
                    .ValueGeneratedNever()
                    .HasColumnName("role_id");

                entity.Property(e => e.Name)
                    .HasMaxLength(250)
                    .HasColumnName("name");
            });

            modelBuilder.Entity<Subject>(entity =>
            {
                entity.ToTable("Subject");

                entity.Property(e => e.SubjectId).HasColumnName("subject_id");

                entity.Property(e => e.Code)
                    .HasMaxLength(50)
                    .HasColumnName("code");

                entity.Property(e => e.Discription)
                    .HasColumnType("text")
                    .HasColumnName("discription");

                entity.Property(e => e.Name)
                    .HasMaxLength(250)
                    .HasColumnName("name");

                entity.Property(e => e.ProfileId).HasColumnName("profileId");

                entity.Property(e => e.Status).HasColumnName("status");

                entity.HasOne(d => d.Profile)
                    .WithMany(p => p.Subjects)
                    .HasForeignKey(d => d.ProfileId)
                    .HasConstraintName("FK_Subject_Profile");
            });

            modelBuilder.Entity<Test>(entity =>
            {
                entity.HasKey(e => e.Code);

                entity.ToTable("Test");

                entity.Property(e => e.Code)
                    .HasMaxLength(250)
                    .HasColumnName("code");

                entity.Property(e => e.CourseId).HasColumnName("course_id");

                entity.Property(e => e.EndTime)
                    .HasColumnType("datetime")
                    .HasColumnName("end_time");

                entity.Property(e => e.Password)
                    .HasMaxLength(250)
                    .HasColumnName("password");

                entity.Property(e => e.StartTime)
                    .HasColumnType("datetime")
                    .HasColumnName("start_time");

                entity.Property(e => e.Status).HasColumnName("status");

                entity.HasOne(d => d.Course)
                    .WithMany(p => p.Tests)
                    .HasForeignKey(d => d.CourseId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Test_Course");

                entity.HasMany(d => d.Flashcards)
                    .WithMany(p => p.Codes)
                    .UsingEntity<Dictionary<string, object>>(
                        "TestFlashCard",
                        l => l.HasOne<FlashCard>().WithMany().HasForeignKey("FlashcardId").OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK_Test_FlashCard_FlashCard"),
                        r => r.HasOne<Test>().WithMany().HasForeignKey("Code").OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK_Test_FlashCard_Test"),
                        j =>
                        {
                            j.HasKey("Code", "FlashcardId");

                            j.ToTable("Test_FlashCard");

                            j.IndexerProperty<string>("Code").HasMaxLength(250).HasColumnName("code");

                            j.IndexerProperty<int>("FlashcardId").HasColumnName("flashcard_id");
                        });
            });

            modelBuilder.Entity<TestProfile>(entity =>
            {
                entity.HasKey(e => new { e.Code, e.ProfileId });

                entity.ToTable("Test_Profile");

                entity.Property(e => e.Code)
                    .HasMaxLength(250)
                    .HasColumnName("code");

                entity.Property(e => e.ProfileId).HasColumnName("profile_id");

                entity.Property(e => e.Mark).HasColumnName("mark");

                entity.Property(e => e.SuccessTime)
                    .HasColumnType("datetime")
                    .HasColumnName("success_time");

                entity.HasOne(d => d.CodeNavigation)
                    .WithMany(p => p.TestProfiles)
                    .HasForeignKey(d => d.Code)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Test_Profile_Test");

                entity.HasOne(d => d.Profile)
                    .WithMany(p => p.TestProfiles)
                    .HasForeignKey(d => d.ProfileId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Test_Profile_Profile");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}

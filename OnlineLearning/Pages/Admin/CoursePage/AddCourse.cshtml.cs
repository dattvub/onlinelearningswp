using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using OnlineLearning.Models;
using OnlineLearning.Utility;

namespace OnlineLearning.Pages.Admin.CoursePage
{
    [Authorize(Roles = RoleConstant.LECTURE)]
    public class AddCourseModel : PageModel
    {
        private readonly SWPContext _context;

        [BindProperty]
        public Course Course { get; set; }
        [BindProperty]
        public int profileId { get; set; }
        public List<Subject> listSub { get; set; }

        public AddCourseModel(SWPContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> OnGetAsync(int id)
        {
            profileId = id;
            listSub = await _context.Subjects.Where(i => i.ProfileId == id).ToListAsync();
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var newCourse = new Course
            {
                Title = Course.Title,
                ProfileId = profileId,
                Status = Course.Status,
                Description = Course.Description,
                Rate = Course.Rate,
                NumberFlashcard = Course.NumberFlashcard,
                SubjectId = Course.SubjectId,
            };
            _context.Courses.Add(newCourse);
            await _context.SaveChangesAsync();
            return RedirectToPage("/Admin/CoursePage/ListCourse", new { id = profileId });
        }
    }
}

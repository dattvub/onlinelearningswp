using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using OnlineLearning.Models;

namespace OnlineLearning.Pages.Admin.CoursePage
{
    public class DeleteCourseModel : PageModel
    {
        private readonly SWPContext _context;

        public DeleteCourseModel(SWPContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> OnGetAsync(int id, int pid)
        {
            var Course = await _context.Courses.FirstOrDefaultAsync(i => i.CourseId == id);
            _context.Courses.Remove(Course);
            await _context.SaveChangesAsync();
            return RedirectToPage("/Admin/CoursePage/ListCourse", new {id = pid});
        }
    }
}

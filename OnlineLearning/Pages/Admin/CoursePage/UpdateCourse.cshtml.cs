using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using OnlineLearning.Models;

namespace OnlineLearning.Pages.Admin.CoursePage
{
    public class UpdateCourseModel : PageModel
    {
        private readonly SWPContext _context;

        [BindProperty]
        public Course Course { get; set; }
        [BindProperty]
        public int? profileId { get; set; }
        public List<Subject> listSub { get; set; }
        public UpdateCourseModel(SWPContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> OnGetAsync(int id, int pid)
        {
            Course = await _context.Courses.FirstOrDefaultAsync(i => i.CourseId == id);
            profileId = pid;
            if (Course == null)
            {
                return NotFound();
            }
            listSub = await _context.Subjects.Where(i => i.ProfileId == pid).ToListAsync();
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var s = await _context.Courses.FirstOrDefaultAsync(i => i.CourseId == Course.CourseId);
            s.Title = Course.Title;
            s.Description = Course.Description;
            s.ProfileId = profileId;
            s.Status = Course.Status;
            s.Rate = Course.Rate;
            s.NumberFlashcard = Course.NumberFlashcard;
            s.SubjectId = Course.SubjectId;

            await _context.SaveChangesAsync();
            return RedirectToPage("/Admin/CoursePage/ListCourse", new { id = profileId });
        }
    }
}

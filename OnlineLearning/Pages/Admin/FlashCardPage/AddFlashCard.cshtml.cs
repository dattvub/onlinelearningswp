using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using OnlineLearning.Models;

namespace OnlineLearning.Pages.Admin.FlashCardPage
{
    public class AddFlashCardModel : PageModel
    {
        private readonly SWPContext _context;

        [BindProperty]
        public FlashCard flashCard { get; set; }
        public List<Course> listCourses { get; set; }
        public List<Answer> listAnswer { get; set; }
        [BindProperty]
        public int ProfileId { get; set; }

        public AddFlashCardModel(SWPContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> OnGetAsync(int id)
        {
            ProfileId = id;
            listAnswer = await _context.Answers.ToListAsync();
            listCourses = await _context.Courses.Where(i => i.ProfileId == id).ToListAsync();
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            _context.FlashCards.Add(flashCard);
            await _context.SaveChangesAsync();
            return RedirectToPage("ListFlashCard", new {id = ProfileId});
        }
    }
}

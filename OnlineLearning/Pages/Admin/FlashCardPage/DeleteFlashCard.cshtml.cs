using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using OnlineLearning.Models;

namespace OnlineLearning.Pages.Admin.FlashCardPage
{
    public class DeleteFlashCardModel : PageModel
    {
        private readonly SWPContext _context;

        public DeleteFlashCardModel(SWPContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> OnGetAsync(int id, int pid)
        {
            var FlashCards = await _context.FlashCards.FirstOrDefaultAsync(i => i.FlashcardId == id);
            _context.FlashCards.Remove(FlashCards);
            await _context.SaveChangesAsync();
            return RedirectToPage("/Admin/FlashCardPage/ListFlashCard", new { id = pid });
        }
    }
}

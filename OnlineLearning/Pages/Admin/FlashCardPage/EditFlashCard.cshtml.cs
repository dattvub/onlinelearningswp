using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using OnlineLearning.Models;

namespace OnlineLearning.Pages.Admin.FlashCardPage
{
    public class EditFlashCardModel : PageModel
    {
        private readonly SWPContext _context;

        [BindProperty]
        public FlashCard flashCard { get; set; }
        public List<Course> listCourses { get; set; }
        public List<Answer> listAnswer { get; set; }
        [BindProperty]
        public int ProfileId { get; set; }

        public EditFlashCardModel(SWPContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> OnGetAsync(int id, int pid)
        {
            ProfileId = pid;
            listAnswer = await _context.Answers.ToListAsync();
            listCourses = await _context.Courses.Where(i => i.ProfileId == pid).ToListAsync();
            flashCard = await _context.FlashCards.FirstOrDefaultAsync(i => i.FlashcardId == id);
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var s = await _context.FlashCards.FirstOrDefaultAsync(i => i.FlashcardId == flashCard.FlashcardId);
            s.Question = flashCard.Question;
            s.AnswerId = flashCard.AnswerId;
            s.CourseId = flashCard.CourseId;
            s.Status = flashCard.Status;

            await _context.SaveChangesAsync();
            return RedirectToPage("/Admin/FlashCardPage/ListFlashCard", new { id = ProfileId });
        }
    }
}

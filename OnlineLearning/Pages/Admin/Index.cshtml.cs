using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using OnlineLearning.Utility;

namespace OnlineLearning.Pages.Admin
{
    [Authorize(Roles = RoleConstant.ADMIN + "," + RoleConstant.LECTURE)]
    public class IndexModel : PageModel
    {
        public void OnGet()
        {
        }
    }
}

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using OnlineLearning.Models;
using OnlineLearning.Utility;

namespace OnlineLearning.Pages.Admin.SubjectPage
{
    [Authorize(Roles = RoleConstant.LECTURE)]
    public class AddSubjectModel : PageModel
    {
        private readonly SWPContext _context;

        [BindProperty]
        public string Name { get; set; }
        [BindProperty]
        public string? Description { get; set; }
        [BindProperty]
        public int ProfileId { get; set; }
        [BindProperty]
        public bool? Status { get; set; }
        [BindProperty]
        public string Code { get; set; } = null!;

        
        public AddSubjectModel(SWPContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> OnGetAsync(int id)
        {
            ProfileId = id;
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var subject = new Subject
            {
                Name = Name,
                ProfileId = ProfileId,
                Status = Status,
                Code = Code,
                Discription = Description
            };
            _context.Subjects.Add(subject);
            await _context.SaveChangesAsync();
            return RedirectToPage("/Admin/SubjectPage/ListSubject", new {id = ProfileId});
        }
    }
}

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using OnlineLearning.Models;

namespace OnlineLearning.Pages.Admin.SubjectPage
{
    public class DeleteSubjectModel : PageModel
    {
        private readonly SWPContext _context;

        public DeleteSubjectModel(SWPContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> OnGetAsync(int id, int pid)
        {
            var subject = await _context.Subjects.FirstOrDefaultAsync(i => i.SubjectId == id);
            _context.Subjects.Remove(subject);
            await _context.SaveChangesAsync();
            return RedirectToPage("/Admin/SubjectPage/ListSubject", new {id = pid});
        }
    }
}

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using OnlineLearning.Models;

namespace OnlineLearning.Pages.Admin.SubjectPage
{
    public class UpdateSubjectModel : PageModel
    {
        private readonly SWPContext _context;

        [BindProperty]
        public Subject Subject { get; set; }


        public UpdateSubjectModel(SWPContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> OnGetAsync(int id)
        {
            Subject = await _context.Subjects.FirstOrDefaultAsync(i => i.SubjectId == id);
            if (Subject == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var s = await _context.Subjects.FirstOrDefaultAsync(i => i.SubjectId == Subject.SubjectId);
            s.Name = Subject.Name;
            s.Code = Subject.Code;
            s.ProfileId = Subject.ProfileId;
            s.Status = Subject.Status;
            s.Discription = Subject.Discription;
            await _context.SaveChangesAsync();
            return RedirectToPage("/Admin/SubjectPage/ListSubject", new { id = Subject.ProfileId });
        }
    }
}

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using OnlineLearning.Models;

namespace OnlineLearning.Pages.Auth
{
    public class ResetPasswordModel : PageModel
    {
        private readonly SWPContext _context;

        public ResetPasswordModel(SWPContext context)
        {
            _context = context;
        }

        public IActionResult OnPost(string email, string old, string neww, string confirm)
        {
            var _user = _context.Accounts.FirstOrDefault(x => x.Email == email && x.Password == old);
            var alert = "";
            if (_user != null)
            {
                if (neww == confirm)
                {
                    _user.Password = neww;
                    _context.SaveChanges();
                    alert = "Change password successful !!";
                }
                else
                {
                    alert = "* New password not equal Confirm password !!!";
                }
            }
            else
            {
                alert = "* Account not exist !!!";
            }
            ViewData["Alert"] = alert;
            return Page();
        }
    }
}


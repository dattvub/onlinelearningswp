using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Security.Claims;
using OnlineLearning.Models;

namespace OnlineLearning.Pages.Auth
{
    public class SignInModel : PageModel
    {
        private readonly SWPContext _context;

        public SignInModel(SWPContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            var accoutId = User.Claims.FirstOrDefault(x => x.Type.Split("/").Last().Equals("name"));
            if (accoutId != null)
            {
                return Redirect("/Home");
            }
            return Page();
        }

        public IActionResult OnGetSignOut()
        {
            HttpContext.SignOutAsync();
            return RedirectToPage("/Auth/SignIn");
        }

        public IActionResult OnPost(string email, string password, [FromQuery] string? ReturnUrl)
        {
            var _user = _context.Accounts.FirstOrDefault(x => x.Email == email && x.Password == password);
            if (_user == null)
            {
                ViewData["error"] = "Username or password wrong !!!";
                return Page();
            }
            else
            {
                var role = _user.RoleId == 1 ? "Admin" : "Student";
                var claims = new List<Claim>() {
                new Claim(ClaimTypes.Name, _user.AccountId.ToString().Trim()),
                new Claim(ClaimTypes.Role, role.ToString().Trim()),
                };
                var identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                var principal = new ClaimsPrincipal(identity);
                HttpContext.SignInAsync(principal, new AuthenticationProperties
                {
                    IsPersistent = true,
                    ExpiresUtc = DateTime.UtcNow.AddDays(7)
                });

                if (_user.RoleId == 3)
                {
                    // Tra ve trang quan ly nhe
                    return RedirectToPage("/SubjectPage/ListSubject");
                }
                else
                {
                    if (_user.RoleId == 1)
                    {
                        return RedirectToPage("/AccountPage/ListAccount");
                    }
                    else
                    {
                        return RedirectToPage("/Home");
                    }
                }
            }
        }
    }
}

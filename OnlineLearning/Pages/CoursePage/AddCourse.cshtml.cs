using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using OnlineLearning.Models;

namespace OnlineLearning.Pages.CoursePage
{
    public class AddCourseModel : PageModel
    {
        private readonly SWPContext _context;

        [BindProperty]
        public int CourseId { get; set; }
        [BindProperty]
        public string Title { get; set; }
        [BindProperty]
        public string Description { get; set; }
        [BindProperty]
        public int ProfileId { get; set; }
        [BindProperty]
        public int SubjectId { get; set; }
        [BindProperty]
        public int NumberFlashcard { get; set; }

        public AddCourseModel(SWPContext context)
        {
            _context = context;
        }


        public async Task<IActionResult> OnPostAsync()
        {
            var course = new Course
            {
                CourseId = CourseId,
                Title = Title,
                Description = Description,
                ProfileId = ProfileId,
                SubjectId = SubjectId,
                NumberFlashcard = NumberFlashcard
            };
            _context.Courses.Add(course);
            await _context.SaveChangesAsync();
            return RedirectToPage("./ListCourse");
        }
    }
}
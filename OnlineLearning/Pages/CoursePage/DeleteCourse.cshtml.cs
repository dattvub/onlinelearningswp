using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using OnlineLearning.Models;

namespace OnlineLearning.Pages.CoursePage
{
    public class DeleteCourseModel : PageModel
    {
        private readonly SWPContext _context;

        public DeleteCourseModel(SWPContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> OnGetAsync(int courId)
        {
            var f = await _context.FlashCards.FirstOrDefaultAsync(i => i.CourseId == courId);
            var course = await _context.Courses.FirstOrDefaultAsync(i => i.CourseId == courId);
            _context.Courses.Remove(course);
            await _context.SaveChangesAsync();


            return RedirectToPage("./ListCourse");
        }
    }
}

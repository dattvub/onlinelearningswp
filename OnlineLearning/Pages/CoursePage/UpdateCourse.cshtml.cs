using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using OnlineLearning.Models;
using System;


namespace OnlineLearning.Pages.CoursePage
{
    public class UpdateCourseModel : PageModel
    {
        private readonly SWPContext _context;

        [BindProperty]
        public Course Course { get; set; }
        [BindProperty(SupportsGet = true)]
        public int PageIndex { get; set; }
        public UpdateCourseModel(SWPContext context)
        {
            _context = context;
        }
        public async Task<IActionResult> OnGetAsync(int courId)
        {
            Course = await _context.Courses.Include(i => i.Subject).FirstOrDefaultAsync(i => i.CourseId == courId);
            if (Course == null) throw new ArgumentException("Can not find Course !");
            return Page();
        }
        public async Task<IActionResult> OnPostAsync()
        {
            var c = await _context.Courses.FirstOrDefaultAsync(i => i.CourseId == Course.CourseId);
            c.Title = Course.Title;
            c.Description = Course.Description;
            c.NumberFlashcard = Course.NumberFlashcard;           
            c.Status= Course.Status;
            await _context.SaveChangesAsync();
            return RedirectToPage("./ListCourse");
        }
    }
}
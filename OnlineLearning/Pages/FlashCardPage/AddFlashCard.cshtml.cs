using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using OnlineLearning.Models;

namespace OnlineLearning.Pages.FlashCardPage
{
    public class AddFlashCardModel : PageModel
    {
        private readonly SWPContext _context;

        [BindProperty]
        public FlashCard flashCard { get; set; }

        public AddFlashCardModel(SWPContext context)
        {
            _context = context;
        }


        public async Task<IActionResult> OnPostAsync()
        {
            _context.FlashCards.Add(flashCard);
            await _context.SaveChangesAsync();
            return RedirectToPage("ViewAllFlashCard");
        }
    }
}

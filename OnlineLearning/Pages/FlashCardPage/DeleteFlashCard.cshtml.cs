using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using OnlineLearning.Models;

namespace OnlineLearning.Pages.FlashCardPage
{
    public class DeleteFlashCardModel : PageModel
    {
        private readonly SWPContext _context;

        public DeleteFlashCardModel(SWPContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> OnGetAsync(int id)
        {
            var flashCard = await _context.FlashCards.FirstOrDefaultAsync(i => i.FlashcardId == id);
            if (flashCard != null)
            {
                _context.FlashCards.Remove(flashCard);
                await _context.SaveChangesAsync();
            }
            return RedirectToPage("ViewAllFlashCard");
        }
    }
}

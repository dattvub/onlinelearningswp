using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using OnlineLearning.Models;

namespace OnlineLearning.Pages.FlashCardPage
{
    public class ListFlashCardModel : PageModel
    {
        private readonly SWPContext _context;

        [BindProperty]
        public string? Search { get; set; }
        public List<FlashCard> listFlashCard { get; set; }

        public ListFlashCardModel(SWPContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> OnGetAsync(string? search, int cid)
        {
            listFlashCard = await _context.FlashCards.Include(i => i.Answer).Where(i => i.CourseId == cid).ToListAsync();
            return Page();
        }

    }
}

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using OnlineLearning.Models;

namespace OnlineLearning.Pages.FlashCardPage
{
    public class UpdateFlashCardModel : PageModel
    {
        private readonly SWPContext _context;

        [BindProperty]
        public FlashCard flashCard { get; set; }
        public List<Answer> listAnswer { get; set; }
        public List<Course> listCourse { get; set; }
        public UpdateFlashCardModel(SWPContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> OnGetAsync(int id)
        {
            flashCard = await _context.FlashCards.Include(i => i.Course).Include(i => i.Answer).FirstOrDefaultAsync(i => i.FlashcardId == id);

            listAnswer = await _context.Answers.ToListAsync();
            listCourse = await _context.Courses.ToListAsync();

            if (flashCard == null) throw new ArgumentException("Can not find subjet !");
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var fl = await _context.FlashCards.FirstOrDefaultAsync(i => i.FlashcardId == flashCard.FlashcardId);
            fl.Question = flashCard.Question;
            fl.AnswerId = flashCard.AnswerId;
            fl.CourseId = flashCard.CourseId;
            fl.Status = flashCard.Status;
            await _context.SaveChangesAsync();
            return RedirectToPage("ViewAllFlashCard");
        }
    }
}

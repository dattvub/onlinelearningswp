using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using OnlineLearning.Models;

namespace OnlineLearning.Pages.FlashCardPage
{
    public class ViewAllFlashCardModel : PageModel
    {
        private readonly SWPContext _context;

        [BindProperty]
        public string? Search { get; set; }
        public List<FlashCard> listAllFlashCard { get; set; }

        public ViewAllFlashCardModel(SWPContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> OnGetAsync(string? search, int cid)
        {
            listAllFlashCard = await _context.FlashCards.Include(i => i.Answer).Include(i => i.Course).ToListAsync();
            return Page();
        }

    }
}

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using OnlineLearning.Models;

namespace OnlineLearning.Pages.SubjectPage
{
    public class AddSubjectModel : PageModel
    {
        private readonly SWPContext _context;

        [BindProperty]
        public int SubjectId { get; set; }
        [BindProperty]
        public string Name { get; set; }
        [BindProperty]
        public int ProfileId { get; set; }
        [BindProperty]
        public bool Status { get; set; }
        [BindProperty]
        public string Code { get; set; } = null!;

        public AddSubjectModel(SWPContext context)
        {
            _context = context;
        }


        public async Task<IActionResult> OnPostAsync()
        {
            var subject = new Subject
            {
                SubjectId = SubjectId,
                Name = Name,
                ProfileId = ProfileId,
                Status = Status,
                Code = Code
            };
            _context.Subjects.Add(subject);
            await _context.SaveChangesAsync();
            return RedirectToPage("./ListSubject");
        }
    }
}

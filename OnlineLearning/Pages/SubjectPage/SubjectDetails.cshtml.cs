using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using OnlineLearning.Models;

namespace OnlineLearning.Pages.SubjectPage
{
    public class SubjectDetailsModel : PageModel
    {


        private readonly SWPContext _context;

        public Course Course { get; set; }
        public Subject Subject { get; set; }
        public SubjectDetailsModel(SWPContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> OnGetAsync(int id)
        {
            Subject = await _context.Subjects.Include(i => i.Profile).FirstOrDefaultAsync(i => i.SubjectId == id);
            Course = await _context.Courses.FirstOrDefaultAsync(i => i.SubjectId == Subject.SubjectId);
            if (Subject == null) throw new ArgumentException("Can not find subjet !");
            return Page();
        }
    }
}

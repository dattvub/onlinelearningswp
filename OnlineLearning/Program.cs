using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.EntityFrameworkCore;
using OnlineLearning.AutoMapper;
using OnlineLearning.Models;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddRazorPages();
builder.Services.AddDbContext<SWPContext>(options => options.UseSqlServer(builder.Configuration.GetConnectionString("MyDbContext")));
builder.Services.AddAutoMapper(typeof(AutoMapperProfile));
/*builder.Services.AddAuthentication(options =>
{
    options.DefaultAuthenticateScheme = "MyAuthenticationScheme"; // Replace with your desired authentication scheme
    options.DefaultChallengeScheme = "MyAuthenticationScheme"; // Replace with your desired authentication scheme
    options.DefaultSignInScheme = "MyAuthenticationScheme"; // Replace with your desired authentication scheme
});*//*.AddCookie("CookieAuthentication", options =>
{
    //options.LoginPath = "/User/Login";
    options.ExpireTimeSpan = TimeSpan.FromHours(23).Add(TimeSpan.FromMinutes(50));
});*/

builder.Services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options =>
                {
                    options.Cookie.Name = "CookieAuthentication";
                    options.Cookie.HttpOnly = true;
                    options.LoginPath = "/Login";
                    options.ExpireTimeSpan = TimeSpan.FromDays(7);
                    // Other configuration options as needed
                });
var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();
app.UseAuthentication();
app.UseAuthorization();

app.MapRazorPages();

app.Run();

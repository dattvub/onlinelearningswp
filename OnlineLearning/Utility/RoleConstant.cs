﻿namespace OnlineLearning.Utility
{
    public static class RoleConstant
    {
        public const string ADMIN = "Admin";
        public const string LECTURE = "Lecture";
        public const string STUDENT = "Student";
    }
}
